Both data and clusters from cmu are used in these data files. For clarity, 'cmuc' in a filename refers to the cmu clusters, while 'cmud' refers to the cmu data.

All word representations in the training, development and test files have been generated using the embeddings and/or clusters files found in speciale/resources/embeddings and speciale/resources/clusters respectively. (Tested July 14 2016=True)

Main:

speciale/data/main/cmu.vw = the concatenation of the two datasets in the CMU repository (daily547, oct27) with honnibal13 features in the .vw format. 
speciale/data/main/cmu_sXXwYY_cmuc.vw = the file above with cmu clusters and embeddings of the type sXXwYY.

Self-training:

All the self-training training files contain cmu clusters and embeddings of the form s20w5.
All the self-training training files contain type constraints of the form specified at the end of the file name (either 'unamb', 'maxXX' or 'all'). The TC measure indicates the tolerated size of constraint (e.g. 'unamb' is essentially labelled data, while 'max12' allows for sentences in the self-training setup, where the full set of POS tags (12) is allowed for eac word in the sentences.
All the self-training training files contain 1500 words, and no costs are included (because the additional in-domain data used in ST is not used at training but at prediction time).
These aspects of the data are not included in the files names since it is the same for all of them (except for the TC type which varies).

All the self-training test files (speciale/data/test/self/*) contain cmu clusters and embeddings of the same format as the training files (s10w5). This is therefore not specified in the file names either.
